package salesforce

import (
	"testing"
	"time"

	"bitbucket.org/evhivetech/salesforce-go/sobjects"
)

func TestCreateLead(t *testing.T) {

	//authenticate
	obj := &SFApi{
		ApiVersion: "v44.0",
		Oauth: &sfOauth{
			AccessToken: "00D0o000001ASfj!AQ4AQDBB5aSH05JbGaSqhnRlCLWep8zFeFb3mVsyjjFVEGDh0OfKezCPT36jaEyvPNY57q9Hfr99_eOXnqOotBySSpr5TyDI",
			InstanceURL: "https://cocowork.my.salesforce.com",
		},
	}

	now := time.Date(2019, 04, 17, 0, 0, 0, 0, time.UTC)
	lead := &sobjects.Lead{
		LeadSorce:        "Broker / Referral Form",
		FirstName:        "Akbar",
		LastName:         "Bakkar",
		Email:            "teserrteeee@yopmail.com",
		PhoneNumber:      "324333224242",
		PlanName:         "Private Office",
		Company:          "MSL",
		NumberOfEmployee: 12,
		MoveInDate:       &now,
		Location:         "The Maja",
		RoomType:         "Flexible Room",
		StayLength:       "1 month",
		Pax:              9,
	}

	leadObj, err := obj.CreateLead(*lead)

	if err != nil {
		t.Fatalf("Unable to authenticate: %#v", err)
	}

	t.Log(leadObj.ID)

	campaign := sobjects.CampaignLead{
		LeadID:     leadObj.ID,
		CampaignID: "7010o000001BsBgAAK",
	}

	err = obj.AssignCampaignToLead(campaign)

	if err != nil {
		t.Fatalf("Unable to authenticate: %#v", err)
	}

}
