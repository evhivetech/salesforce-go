package sobjects

//CampaignLead - add lead to campagin
type CampaignLead struct {
	LeadID     string `json:"LeadID"`
	CampaignID string `json:"CampaignID"`
}
