package sobjects

import "time"

//Lead - lead oject for sales force
type Lead struct {
	LeadSorce        string     `json:"LeadSource,omitempty"`
	FirstName        string     `json:"FirstName,omitempty"`
	LastName         string     `json:"LastName,omitempty"`
	Email            string     `json:"Email,omitempty"`
	PhoneNumber      string     `json:"Phone,omitempty"`
	PlanName         string     `json:"Plans__c,omitempty"`
	Company          string     `json:"Company,omitempty"`
	NumberOfEmployee int        `json:"NumberOfEmployees,omitempty"`
	MoveInDate       *time.Time `json:"Lease_Start_Date__c,omitempty"`
	Location         string     `json:"Location_Lead__c,omitempty"`
	RoomType         string     `json:"Rooms__c,omitempty"`
	StayLength       string     `json:"Length_Of_Stay__c,omitempty"`
	Pax              int        `json:"Pax__c,omitempty"`
	Campaign         string     `json:"Campaign_ID__c,omitempty"`
	Notes            string     `json:"Notes__c,omitempty"`
}

//LeadResponse - response of create lead
type LeadResponse struct {
	ID      string `json:"id"`
	Success bool   `json:"success"`
}
